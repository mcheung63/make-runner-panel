make-runner-panel
=================

Execute any make command in [Atom](http://atom.io).

This plugin provides a tree view on the right hand side, the tree nodes are the targets in the Makefile. When you click on it, it will open a xterm and run the make command for you.

`ctrl-alt-o` to toggle panel, it auto scan the Makefile in the first level of directory of all projects

![Demo](https://raw.githubusercontent.com/mcheung63/make-runner-panel/master/screenshots/make-runner-panel.gif)

Contact : Peter, mcheung63@hotmail.com
