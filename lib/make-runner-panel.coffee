MakeRunnerPanelView = require './make-runner-panel-view'
{CompositeDisposable} = require 'atom'
{XRegExp} = require 'xregexp'
TreeView = require './tree-view'
path = require 'path'
fs = require('fs')

targetRegExp = new XRegExp '^[a-z0-9 -]+:'

module.exports = MakeRunner =
  makeRunnerPanelView: null
  bottomPanel: null
  subscriptions: null
  config:
    'xterm path':
        type: 'string'
        default: if process.platform is 'win32'
            'xterm.exe'
          else if process.platform is 'linux'
            '/usr/bin/xterm'
          else if process.platform is 'darwin'
          else
            '/opt/X11/bin/xterm'

  activate: (state) ->
    @makeRunnerPanelView = new MakeRunnerPanelView(state.makeRunnerPanelViewState)
    @bottomPanel = atom.workspace.addBottomPanel(item: @makeRunnerPanelView.getElement(), visible: false)
    #@bottomPanel.show()

    # Events subscribed to in atom's system can be easily cleaned up with a CompositeDisposable
    @subscriptions = new CompositeDisposable

    # Register command that toggles this view
    @subscriptions.add atom.commands.add 'atom-workspace', 'make-runner-panel:toggle': => @toggle()
    @treeView = new TreeView @getMatches()
    #@treeView.attach()

  deactivate: ->
    @bottomPanel.destroy();
    @subscriptions.dispose()
    @makeRunnerPanelView.destroy()

  toggle: ->
      if @treeView.isVisible()
          @treeView.detach()
      else
          @treeView.attach()

  getMatches: ->
      text = "peter"
      root = []
      for directory in atom.project.getDirectories()
        projectNode=new ProjectEntry path.basename(directory.path)
        if fs.existsSync directory.path+'/Makefile'
          fileContent=fs.readFileSync(directory.path+'/Makefile', 'utf8')
          lines=fileContent.split("\n");
          sortedTargetsFirst=[]
          sortedTargetsSecond=[]
          for line in lines.sort()
            if target = XRegExp.exec line, targetRegExp, 0
              targetName=XRegExp.replace(target, XRegExp(' *:'), '')
              if targetName=="all" || targetName=="clean" | targetName=="run"
                sortedTargetsFirst.push targetName
              else
                sortedTargetsSecond.push targetName
          for target in sortedTargetsFirst.sort()
            projectNode.targets.push new TargetEntry target, directory.path
          for target in sortedTargetsSecond.sort()
              projectNode.targets.push new TargetEntry target, directory.path


          root.push projectNode
          attach()
        else
          atom.notifications.addWarning("make-runner-panel : No makefile in current project, click ctrl-alt-o when you are in a Makefile project.")
      matches = root

ProjectEntry = (name) ->
  @targets = []
  @icon = 'icon-project'
  @name = name
  @type = 'project'
  @isExpanded = true
  @isRoot = true
  return

TargetEntry = (name, dir) ->
  @arguments = []
  @dir = dir
  @icon = 'icon-target'
  @name = name
  @isExpanded = false
  @isRoot = false
  return
