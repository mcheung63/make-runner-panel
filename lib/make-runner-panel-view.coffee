module.exports =
class MakeRunnerView
  constructor: (serializedState) ->
    # Create root element
    @element = document.createElement('div')
    @element.classList.add('make-runner-panel')

    # Create message element
    message = document.createElement('div')
    message.setAttribute("id", "Div1");
    message.textContent = "Peter The MakeRunner package is Alive! It's ALIVE!"
    message.classList.add('message')
    @element.appendChild(message)

  # Returns an object that can be retrieved when package is activated
  serialize: ->

  # Tear down any state and detach
  destroy: ->
    @element.remove()

  getElement: ->
    @element
