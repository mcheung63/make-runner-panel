var __hasProp = {}.hasOwnProperty,
	__extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	$ = require('atom-space-pen-views').$,
	View = require('atom-space-pen-views').View;

module.exports = NodeView = (function (parent) {
	__extends(NodeView, parent);

	function NodeView () {
		NodeView.__super__.constructor.apply(this, arguments);
	}

	NodeView.content = function () {
		return this.li({
			'class': 'list-nested-item collapsed'
		}, function () {
			this.div({
				'class': 'header list-item',
				'outlet': 'header'
			}, function () {
				return this.span({
					'class': 'name icon',
					'outlet': 'name'
				})
			}.bind(this));
			this.ol({
				'class': 'entries list-tree',
				'outlet': 'entries'
			});
		}.bind(this));
	};

	NodeView.prototype.initialize = function (tree) {
		var self = this;

		self.item = tree;
		self.name.text(self.item.name);

		if (self.item.icon=='icon-project'){
			self.name.addClass(self.item.icon);
		} if ((self.item.name=="all" || self.item.name=="clean" || self.item.name=="run") && self.item.icon=='icon-target'){
			self.name.addClass(self.item.icon);
		}

		if (self.item.isExpanded || self.item.isRoot){
			self.expand();
		}

		self.setClasses();
		self.repaint();

		// Events
		self.on('click', function (e) {
			e.stopPropagation();

			if (!self.item.isExpanded) {
				self.expand();
			}else{
				self.collapse();
			}

			/*if (self.item.isExpanded || self.item.isRoot){
				self.expand();
			}*/

			self.setClasses();
			if (self.item.icon=='icon-target'){
				var xtermPath=atom.config.get('make-runner-panel.xterm path');
				var spawn = require('child_process').spawn;


				var commandStart='. ~/.bashrc;cd '+self.item.dir;
				var command=';make '+self.item.name;
				var commandEnd=';echo "DONE, HIT ENTER TO QUIT";read a;';
				var child = spawn(xtermPath, ['-geo', '160x60-700-500', '-e', commandStart+command+commandEnd]);

				/*
				var commandStart='echo $0;cd '+self.item.dir;
				var command=';make '+self.item.name;
				var commandEnd='';
				var child = spawn(xtermPath, ['-geo', '160x60-10-500', '-hold', '-e', '/bin/bash -v -l -c -i "echo $JAVA_HOME"']);
				*/

				child.stdout.on('data',
				    function (data) {
				        console.log(data);
				    }
				);
			}
		});

		self.on('dblclick', function (e) {
			e.stopPropagation();

			var view = $(this).view();
			if (!view)
				return
		});

		// make the node display as leaf, so it can't be expand
		if (self.item.icon == 'icon-target') {
			self.removeClass('list-nested-item');
			self.addClass('list-item');
		}
	}

	NodeView.prototype.repaint = function (recursive) {
		var self = this,
			items = [];

		self.entries.children().detach();

		if (self.item.type == 'project'){
			if (self.item.targets.length == 0) {
				self.removeClass('list-nested-item');
				self.addClass('list-item');
			}

			self.item.targets.forEach(function(target){
				items.push(new NodeView(target));
			});
		}

		views = items;

		views.forEach(function (view) {
			self.entries.append(view);
		});
	}

	NodeView.prototype.setClasses = function () {
		if (this.item.isExpanded) {
			this.addClass('expanded').removeClass('collapsed');
		}else {
			this.addClass('collapsed').removeClass('expanded');
		}
	}

	NodeView.prototype.expand = function (recursive) {
		console.log('expand');
		this.item.isExpanded = true;

		if (recursive) {
			this.entries.children().each(function () {
				var view = $(this).view();
				if (view && view instanceof NodeView)
					view.expand(true);
			});
		}
	}

	NodeView.prototype.collapse = function (recursive) {
		this.item.isExpanded = false;

		if (recursive) {
			this.entries.children().each(function () {
				var view = $(this).view();
				if (view && view instanceof NodeView)
					view.collapse(true);
			});
		}
	}

	NodeView.prototype.toggle = function (recursive) {
		if (this.item.isExpanded)
			this.collapse(recursive);
		else
			this.expand(recursive);
	}


	return NodeView;

})(View);
